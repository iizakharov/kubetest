FROM openjdk:8-jre-alpine

COPY target/kubetest-1.0.jar /usr/lib/

EXPOSE 8000

CMD ["/usr/bin/java", "-jar", "/usr/lib/kubetest-1.0.jar", "1"]