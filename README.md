Build docker image <https://docs.docker.com/engine/reference/commandline/build/>
```bash
docker build -t kubetest .
```

Run docker container <https://docs.docker.com/engine/reference/commandline/run/>
```bash
docker run --rm -p 8000:8000 kubetest 
```