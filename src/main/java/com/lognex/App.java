package com.lognex;

import com.sun.net.httpserver.HttpServer;

import java.io.OutputStream;
import java.net.InetSocketAddress;

public final class App {
    private App() {}

    public static void main(String[] args) throws Exception {
        String version = args.length > 0 ? args[0] : "0";
        HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);
        server.createContext("/version", e -> {
            String response = "Version=" + version;
            e.sendResponseHeaders(200, response.getBytes().length);
            OutputStream os = e.getResponseBody();
            os.write(response.getBytes());
            os.close();
            System.out.println(e.getRequestMethod() + " " + e.getRequestURI());
        });
        server.createContext("/health", e -> {
           e.sendResponseHeaders(200, 0L);
           e.getResponseBody().close();
            System.out.println(e.getRequestMethod() + " " + e.getRequestURI());
        });
        server.setExecutor(null); // creates a default executor
        server.start();
        System.out.println("Server started, Version=" + version);
    }
}
